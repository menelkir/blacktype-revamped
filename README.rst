blacktype-revamped
==================

A custom theme based on blacktype with custom icons

.. image:: https://gitlab.com/menelkir/blacktype-revamped/raw/master/screenshot.png

===========
Themes used
===========

:blacktype: https://github.com/xenatt/CloverTheme/tree/master/blacktype
:refind-menelkir: http://gitlab.com/menelkir/refind-menelkir

============
Installation
============

1) Copy to your clover theems directory (usually in EFI/clover/themes/)

2) Edit your config.plist to use this theme

*<key>Theme</key>*

*<string>blacktype-revamped</string>*

=============
How to donate
=============

If you find this repo useful (don't forget to pay a visit to the related
repos too), you can buy me a beer:

:BTC: 3ECzX5UhcFSRv6gBBYLNBc7zGP9UA5Ppmn

:ETH: 0x7E17Ac09Fa7e6F80284a75577B5c1cbaAD566C1c
