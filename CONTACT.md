## Contact Information

### Maintainers

 * Daniel Menelkir <menelkir@itroll.org>

### Bugs/issues/Whatever else

 * Please use the [issues page](https://github.com/menelkir/refind-menelkir/issues).
